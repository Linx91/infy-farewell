{
var data = [
    {  "name": "Abhishek", "tag": "#Mentor #Inspiring-coder #Great-Advisor", "body": "Thank you for mentoring me as I started on my career nearly 6 years ago.", "footer": "Thank you Abhishek! " } ,
    {  "name": "Ajit", "tag": "#Philosopher #Amazing-classics-Singer #Incredible-Experiences-Owner(of which #1 is Tsunami escapade)", "body": "Your look on life is funny but thoughtful and wise. ", "footer": "Thank you for being you, Ajit! " } ,
    {  "name": "Ajith", "tag": "#1-Manager #Strategic-Operative #Passion-Instigator ", "body": "You were my role model for a passion driven work ethic. I consider it a great honour and privilege working for you.", "footer": "Thank you Ajith! " } ,
    {  "name": "Anuradha", "tag": "#Coding-Mentor #Inspiring-Teacher ", "body": "Your Sharepoint 2010 branding training session that I attending in 2013 laid the foundation for my choice of career field. You have been there, every time I was stuck technically and came to you for help. ", "footer": "Thank you for being an inspiration mam! " } ,
    {  "name": "Babu", "tag": "#Driven #Risk-Taker #Challenging", "body": "Your life is an example that challenges need not bring one down but one can always rise above it. ", "footer": "All the very best Babu! " } ,
    {  "name": "Chandrasena", "tag": "#Great-Singer #Witty #Passionate-Learner ", "body": "Your mind-set to keep learning new skills is one of your great strengths.", "footer": "All the very best Chandra! " } ,
    {  "name": "Farhana", "tag": "#Friendly #Helpful #Considerate ", "body": "You guided me well as I started on my career as a newbie", "footer": "Thank you Farhana!" } ,
    {  "name": "Girija", "tag": "#Hard-worker #Passionate-socialiser #Skilled-Operative ", "body": "You have myriads of talents; your Gulab Jamon would always stand out for me. ", "footer": "All the very best Girija! " } ,
    {  "name": "Hemalatatha", "tag": "#Bubbly #Sporty #Active", "body": "Your hearty laugh cannot be forgotten. Keep smiling!", "footer": "All the very best Hema! " } ,
    {  "name": "Imraz", "tag": "#Smart #One-man-army #Funny", "body": "You can do many things - pick codes crazy fast, make subtle jokes, run your life out to help people (especially my charger).", "footer": "All the very best Imraz!" } ,
    {  "name": "Jebin ", "  tag": "#God-fearing #Helpful #Energetic", "body": "You made me comfortable at BF. May God use you to bring many more in fellowship with the saints.", "footer": "Wishing you the very best of Grace!" } ,
    {  "name": "John", "tag": "#God-fearing #Sporty #Helpful", "body": "Thanks for reminding me of BF meets and making me feel comfortable there. Wish you win many more medals at Marathons. ", "footer": "Wishing you the very best of Grace!" } ,
    {  "name": "Johnson", "tag": "#Big-Boss-Buff", "body": "I’ve learnt a lot from you.", "footer": "Thank you John!" } ,
    {  "name": "Kripakaran", "tag": "#Funny #Helpful #Prudent", "body": "Your helping nature is admirable. Will miss the laughs I've had with you. ", "footer": "All the very best Anna!" } ,
    {  "name": "Litty", "tag": "#Great-team-leader #Simple #Hard-Working", "body": "Your work-style is inspiring. T'was a pleasure working with you.", "footer": "Thank you Litty!" } ,
    {  "name": "Mamatha", "tag": "#Silent-Observer #Bubbly #Strong-Lady", "body": "The way you handle life with a smile is simple amazing.", "footer": "All the very best Mamtha! " } ,
    {  "name": "Mayur", "tag": "#Well-rounded-Standup-Comedian #Witty #Talented ", "body": "Your visit to our place brought us free sessions of laughter therapy.", "footer": "Thank you Mayur!" } ,
    {  "name": "Meena", "tag": "#Kind #Helpful #Very-Understanding", "body": "You have helped me in so many things and above all you have been an inpiration at work.", "footer": "Thank you Meena!" } ,
    {  "name": "Merlin", "tag": "#Creative-event-planner #Artsy #Mature-Motivator ", "body": "You play a huge role in making the work place a wonderful one for all.", "footer": "Thank you Merlin! " } ,
    {  "name": "Venkateshwara", "tag": "#Social #Humourous", "body": "Haven’t worked with you much, but the few conversations we had, told me that you were a fun person to work with.", "footer": "All the very best Venkatesh! " } ,
    {  "name": "Paul", "tag": "#Pillar-of-Finacle #Biiig-heart #Best-name-generator", "body": "Sir, you have helped me in so many ways. You were always there when I needed you.", "footer": "Thank you sir!" } ,
    {  "name": "Prachi", "tag": "#BFF #CodingMentor #Partner-in-crime", "body": "You were more than a walking-google to me. Life at office was so much easier because of you. ", "footer": "Thank you for being you, Prachi! " } ,
    {  "name": "Pradeep", "tag": "#Hard-Worker #Self-sustained #Quiet-but-quick-thinker ", "body": "You work so much without making any noise is exemplary.", "footer": "All the very best Pradeep!" } ,
    {  "name": "Praghathi", "tag": "#Best-Buddy #Partner-in-crime #Motivated-Achiever ", "body": "The way you push yourself to achieve your goals is very inspiring. Being around you was enjoyable. ", "footer": "Thank you for being you, Lady! " } ,
    {  "name": "Rahul", "tag": "#Creative #Fast #Smart", "body": "You were fast in everything - coming to Infy, catching up with code at Infy, leaving Infy. Godspeed bro! ", "footer": "All the very best Rahul!" } ,
    {  "name": "Rajnish", "tag": "#Logical-thinker #Smart #Financial-Consultant", "body": "Thank you for all the advise you have given me both professionally and personally. ", "footer": "All the very best Rajnish!" } ,
    {  "name": "Ramu", "tag": "#Witty #Mood-Lightener #Intelligent", "body": "You can turn something boring into the talk-of-the-town. ", "footer": "All the very best Ramu! " } ,
    {  "name": "Ravi", "tag": "#Smart #Funny", "body": "You were clear in your requiements. T'was a pleasure working with you.", "footer": "Best of Grace Ravi!" } ,
    {  "name": "Rekha", "tag": "#Funny #Lovable #Crazy-Driver", "body": "Have had a quite a lot memorable laughs with you. T'was a pleasure working with you.", "footer": "Thank you Rekha! " } ,
    {  "name": "Roopesh", "tag": "#Friendly #Driven #Helpful ", "body": "Thank you Roopesh for all your help and support. Hope I have complied with all the Jamon treats! ", "footer": "All the very best Roopesh! " } ,
    {  "name": "Sachin", "tag": "#Smart #Strategic-Thinker #Friend", "body": "It was enjoyable beign around you. Your idea of life was simple but effective.", "footer": "All the very best Sachin!" } ,
    {  "name": "Satheesha", "tag": "#Philosophical #Driven #Passionate", "body": "You have a talent for entertaining conversations. ", "footer": "All the very best Satheesha!" } ,
    {  "name": "Segu", "tag": "#Great-Mentor #Strategic-Leader #Email-Teacher", "body": "I will never forget the chai times you spent for me mentoring me on my career path. I owe my E-mail writing skills to you as well.", "footer": "Thank you Segu!" } ,
    {  "name": "Sashi", "tag": "#Buddy #Bubbly #Smart", "body": "Your kind heart, smart brain and skillful work will take you to greater heights. ", "footer": "All the very best Sashi" } ,
    {  "name": "Syamala", "tag": "#Best-Buddy #Kokhla-head #Sweetest-thing", "body": "You are a friend, advisor, chai-buddy, enourager and many more. ", "footer": "Thank you for being you Syam!" } ,
    {  "name": "Smita", "tag": "#Smart #Bus-buddy", "body": "Discussing life with you was amusing as well as thought provoking. Will miss all the bus chats we have had recently.", "footer": "All the very best Smitha!" } ,
    {  "name": "Sowmiyaa", "tag": "#Smart #Quick-Leaner #Team-Player", "body": "You bubbly nature and smart work will add so much value to the team. ", "footer": "All the very best Sowmiyaa!" } ,
    {  "name": "Sudhakara", "tag": "#Simple #Friendly #Plant-Advisor", "body": "Chatting about gardening with you, playing pranks, Andhra meals were all very enjoyable moments. ", "footer": "All the vert best Ranjith!" } ,
    {  "name": "Suryakiran", "tag": "#Wise #Knowlegable #Driven", "body": "Conversations with you have always been informative.", "footer": "All the very best Surya!" } ,
    {  "name": "Suryasnata", "tag": "#Bubbly #Ever-smiling #Sweet", "body": "Your smile was always heart warming. ", "footer": "All the very best Surya! " } ,
    {  "name": "Veena", "tag": "#Coding-Mentor #Inspiring-Teacher ", "body": "Your AngularJS training session that I attending in 2017 was very informative and it put me on the right career path. ", "footer": "Thank you for introducing me to Angular mam!" } ,
    {  "name": "Vinod", "tag": "#Creative-event-Planner", "body": "I’ve learnt a lot from you.", "footer": "Thank you Vinod! " } ,
    
]

$('#MsgModal').modal({ show: false});

function printMsg (){
    let user = userName.value.trim();
    console.log(user);

    let result= data.filter(x=>x.name.toLowerCase()==user.toLowerCase());
    console.log(result);

    $("#MsgModal").modal('show');

    if (result.length)
    {
        console.log($("#msgBody"));
        $("#msgHeader").text(result[0].name);
        $("#msgTag").text(result[0].tag);
        $("#msgBody").text(result[0].body);
        $("#msgFooter").text(result[0].footer);
    }    
    else 
    {
        $("#msgHeader").text("");
        $("#msgTag").text("");
        $("#msgBody").text("Oops! Did you get your name right?");
        $("#msgFooter").text("");
    }
    
}

var userName = document.getElementById("username");
userName.addEventListener("keyup",function(event){
    event.preventDefault();
    if(event.keyCode == 13){
        document.getElementById("btnSelectUser").click();
    }
})

}